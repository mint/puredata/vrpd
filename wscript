#!/usr/bin/env python

import sys

def options(opt):
        opt.load('compiler_cxx')

def configure(conf):
        conf.load('compiler_cxx')
        conf.env.CXXFLAGS = ['-DFLEXT_SYS=2', '-DPD', '-DFLEXT_USE_CMEM',
                             '-O6', '-fno-rtti' ,'-Wall','-DFLEXT_SHARED']
        conf.env.LIB = ['m', 'Leap', 'flext-pd', 'vrpn', 'quat']
        conf.env.LIBPATH = ['/usr/lib','/usr/local/lib']
        if sys.platform == 'darwin':
            conf.env.INCLUDES_OS = ['/Applications/Pd-extended.app/Contents/Resources/include', '/usr/local/include/flext']
            conf.env.CXXFLAGS_OS = ['-m32']
            conf.env.LINKFLAGS_OS = ['-m32','-flat_namespace']
            conf.env.cxxshlib_PATTERN = '%s.pd_darwin'
        else :
            conf.env.cxxshlib_PATTERN = '%s.pd_linux'


def build(bld):
    #TRACKER
    bld.shlib(
     source       = 'vrpd_tracker/vrpd_tracker.cpp',
     use          = ['OS','VRPD'],
     target       = 'vrpd_tracker',
     )
    #BUTTON
    bld.shlib(
     source       = 'vrpd_button/vrpd_button.cpp',
     use          = ['OS','VRPD'],
     target       = 'vrpd_button',
     )
    #ANALOG
    bld.shlib(
     source       = 'vrpd_analog/vrpd_analog.cpp',
     use          = ['OS','VRPD'],
     target       = 'vrpd_analog',
     )

